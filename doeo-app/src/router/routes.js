const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'AllOrganizations', component: () => import('pages/AllOrganizations.vue') },
      { path: '', redirect: '/AllOrganizations' },
      { path: 'Organization/:id', component: () => import('pages/OrganizationInformation.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
