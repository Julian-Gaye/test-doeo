<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240311130408 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE app_user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE organization_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE reminder_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE talk_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_doeo_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE app_user (id INT NOT NULL, organization_id INT NOT NULL, last_name VARCHAR(45) NOT NULL, first_name VARCHAR(45) NOT NULL, status VARCHAR(45) NOT NULL, last_login TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_88BDF3E932C8A3DE ON app_user (organization_id)');
        $this->addSql('CREATE TABLE organization (id INT NOT NULL, name VARCHAR(45) NOT NULL, status VARCHAR(45) NOT NULL, referent_name VARCHAR(45) NOT NULL, referent_function VARCHAR(45) NOT NULL, referent_mail VARCHAR(45) NOT NULL, referent_phone VARCHAR(45) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE reminder (id INT NOT NULL, organization_id INT NOT NULL, user_id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, status VARCHAR(45) NOT NULL, note VARCHAR(45) NOT NULL, due_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_40374F4032C8A3DE ON reminder (organization_id)');
        $this->addSql('CREATE INDEX IDX_40374F40A76ED395 ON reminder (user_id)');
        $this->addSql('CREATE TABLE talk (id INT NOT NULL, user_id INT NOT NULL, organization_id INT NOT NULL, datetime TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, note TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9F24D5BBA76ED395 ON talk (user_id)');
        $this->addSql('CREATE INDEX IDX_9F24D5BB32C8A3DE ON talk (organization_id)');
        $this->addSql('CREATE TABLE user_doeo (id INT NOT NULL, username VARCHAR(45) NOT NULL, fullname VARCHAR(45) NOT NULL, email VARCHAR(45) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE app_user ADD CONSTRAINT FK_88BDF3E932C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE reminder ADD CONSTRAINT FK_40374F4032C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE reminder ADD CONSTRAINT FK_40374F40A76ED395 FOREIGN KEY (user_id) REFERENCES user_doeo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE talk ADD CONSTRAINT FK_9F24D5BBA76ED395 FOREIGN KEY (user_id) REFERENCES user_doeo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE talk ADD CONSTRAINT FK_9F24D5BB32C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
       
        //Insert data
        $this->addSql("INSERT INTO public.organization (id, name, status, referent_name, referent_function, referent_mail, referent_phone) VALUES (3, 'Julian', 'En cours', 'Referent', 'Fonction', 'mail@mail.com', '06123456789')");
        $this->addSql("INSERT INTO public.organization (id, name, status, referent_name, referent_function, referent_mail, referent_phone) VALUES (4, 'John', 'En cours', 'Referent', 'Fonction', 'john@mail.com', '0601010101')");
        $this->addSql("INSERT INTO public.organization (id, name, status, referent_name, referent_function, referent_mail, referent_phone) VALUES (5, 'Paul', 'Terminé', 'Referent', 'Fonction', 'paul@mail.com', '06987654321')");

        $this->addSql("INSERT INTO public.user_doeo (id, username, fullname, email) VALUES (1, 'Julian', 'Julian Gaye', 'mail@mail.com')");

        $this->addSql("INSERT INTO public.talk (id, user_id, organization_id, datetime, note) VALUES (4, 1, 3, '2024-03-13 12:09:54', 'Talk 1')");
        $this->addSql("INSERT INTO public.talk (id, user_id, organization_id, datetime, note) VALUES (5, 1, 3, '2024-03-13 12:09:54', 'Talk 2')");

        $this->addSql("INSERT INTO public.app_user (id, organization_id, last_name, first_name, status, last_login, created_at, deleted_at) VALUES (1, 3, 'Gaye', 'Julian', 'En cours', '2024-03-12 20:19:21', '2024-03-12 20:19:21', '2024-03-12 20:19:21')");

        $this->addSql("INSERT INTO public.reminder (id, organization_id, user_id, created_at, status, note, due_date) VALUES (1, 3, 1, '2024-03-13 12:49:53', 'En cours', 'Rappel 1', '2024-03-13 12:49:53')");
        $this->addSql("INSERT INTO public.reminder (id, organization_id, user_id, created_at, status, note, due_date) VALUES (2, 3, 1, '2024-03-13 12:49:53', 'En attente de réponse', 'Rappel 2', '2024-03-13 12:49:53')");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE app_user_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE organization_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE reminder_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE talk_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_doeo_id_seq CASCADE');
        $this->addSql('ALTER TABLE app_user DROP CONSTRAINT FK_88BDF3E932C8A3DE');
        $this->addSql('ALTER TABLE reminder DROP CONSTRAINT FK_40374F4032C8A3DE');
        $this->addSql('ALTER TABLE reminder DROP CONSTRAINT FK_40374F40A76ED395');
        $this->addSql('ALTER TABLE talk DROP CONSTRAINT FK_9F24D5BBA76ED395');
        $this->addSql('ALTER TABLE talk DROP CONSTRAINT FK_9F24D5BB32C8A3DE');
        $this->addSql('DROP TABLE app_user');
        $this->addSql('DROP TABLE organization');
        $this->addSql('DROP TABLE reminder');
        $this->addSql('DROP TABLE talk');
        $this->addSql('DROP TABLE user_doeo');
    }
}
