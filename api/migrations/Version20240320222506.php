<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240320222506 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_user ALTER organization_id DROP NOT NULL');
        $this->addSql('ALTER TABLE app_user ALTER last_login DROP NOT NULL');
        $this->addSql('ALTER TABLE app_user ALTER deleted_at DROP NOT NULL');
        $this->addSql('ALTER TABLE reminder ALTER organization_id DROP NOT NULL');
        $this->addSql('ALTER TABLE reminder ALTER user_id DROP NOT NULL');
        $this->addSql('ALTER TABLE reminder ALTER status DROP NOT NULL');
        $this->addSql('ALTER TABLE reminder ALTER note DROP NOT NULL');
        $this->addSql('ALTER TABLE talk ALTER user_id DROP NOT NULL');
        $this->addSql('ALTER TABLE talk ALTER organization_id DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE talk ALTER user_id SET NOT NULL');
        $this->addSql('ALTER TABLE talk ALTER organization_id SET NOT NULL');
        $this->addSql('ALTER TABLE app_user ALTER organization_id SET NOT NULL');
        $this->addSql('ALTER TABLE app_user ALTER last_login SET NOT NULL');
        $this->addSql('ALTER TABLE app_user ALTER deleted_at SET NOT NULL');
        $this->addSql('ALTER TABLE reminder ALTER organization_id SET NOT NULL');
        $this->addSql('ALTER TABLE reminder ALTER user_id SET NOT NULL');
        $this->addSql('ALTER TABLE reminder ALTER status SET NOT NULL');
        $this->addSql('ALTER TABLE reminder ALTER note SET NOT NULL');
    }
}
