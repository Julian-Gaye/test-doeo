## Pré-requis

Pour utiliser l'application, il faut télécharger [Docker](https://www.docker.com/products/docker-desktop/)

## Lancer l'application

Se déplacer à la racine du projet

Lancer la commande suivante :

```sh
docker composer up
```

Après quelques instants, l'application est accessible sur http://localhost:9000/

# Navigation dans l'application

## Liste des organisations
La liste des organisations est disponible via le lien suivant : http://localhost:9000/#/AllOrganizations.

Il est possible de cliquer sur n'importe quelle organisation dans le tableau pour ouvrir ses détails.

## Détails d'une organisation
La page des détails d'une organisation est disponible en cliquant sur une ligne sur la [page de toutes les organisation](http://localhost:9000/#/AllOrganizations) ou via le lien suivant : http://localhost:9000/#/organization/{id}. \
Il suffit de remplacer {id} par l'id de l'organisation.

Par exemple le lien http://localhost:9000/#/organization/3 ouvre le détail de l'organisation ayant l'id 3.

# Données

Des données de test sont insérées dans la base de données au lancement de l'application.

Si aucune donnée n'est affichée sur les différentes pages de l'application, il faut probablement attendre quelques minutes que le backend soit complètement lancé pour accéder aux données.