import { NextComponentType, NextPageContext } from "next";
import Head from "next/head";

import { Form } from "../../components/talk/Form";

const Page: NextComponentType<NextPageContext> = () => (
  <div>
    <div>
      <Head>
        <title>Create Talk</title>
      </Head>
    </div>
    <Form />
  </div>
);

export default Page;
