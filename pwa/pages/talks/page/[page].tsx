import { GetStaticPaths, GetStaticProps } from "next";
import { dehydrate, QueryClient } from "react-query";

import {
  PageList,
  getTalks,
  getTalksPath,
} from "../../../components/talk/PageList";
import { PagedCollection } from "../../../types/collection";
import { Talk } from "../../../types/Talk";
import { fetch, getCollectionPaths } from "../../../utils/dataAccess";

export const getStaticProps: GetStaticProps = async ({
  params: { page } = {},
}) => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(getTalksPath(page), getTalks(page));

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<Talk>>("/talks");
  const paths = await getCollectionPaths(
    response,
    "talks",
    "/talks/page/[page]"
  );

  return {
    paths,
    fallback: true,
  };
};

export default PageList;
