import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Form } from "../../../components/talk/Form";
import { PagedCollection } from "../../../types/collection";
import { Talk } from "../../../types/Talk";
import { fetch, FetchResponse, getItemPaths } from "../../../utils/dataAccess";

const getTalk = async (id: string | string[] | undefined) =>
  id ? await fetch<Talk>(`/talks/${id}`) : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const { data: { data: talk } = {} } = useQuery<
    FetchResponse<Talk> | undefined
  >(["talk", id], () => getTalk(id));

  if (!talk) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{talk && `Edit Talk ${talk["@id"]}`}</title>
        </Head>
      </div>
      <Form talk={talk} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["talk", id], () => getTalk(id));

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<Talk>>("/talks");
  const paths = await getItemPaths(response, "talks", "/talks/[id]/edit");

  return {
    paths,
    fallback: true,
  };
};

export default Page;
