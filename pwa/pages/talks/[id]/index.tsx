import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/talk/Show";
import { PagedCollection } from "../../../types/collection";
import { Talk } from "../../../types/Talk";
import { fetch, FetchResponse, getItemPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getTalk = async (id: string | string[] | undefined) =>
  id ? await fetch<Talk>(`/talks/${id}`) : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const { data: { data: talk, hubURL, text } = { hubURL: null, text: "" } } =
    useQuery<FetchResponse<Talk> | undefined>(["talk", id], () => getTalk(id));
  const talkData = useMercure(talk, hubURL);

  if (!talkData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show Talk ${talkData["@id"]}`}</title>
        </Head>
      </div>
      <Show talk={talkData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["talk", id], () => getTalk(id));

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<Talk>>("/talks");
  const paths = await getItemPaths(response, "talks", "/talks/[id]");

  return {
    paths,
    fallback: true,
  };
};

export default Page;
