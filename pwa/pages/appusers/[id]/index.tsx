import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/appuser/Show";
import { PagedCollection } from "../../../types/collection";
import { AppUser } from "../../../types/AppUser";
import { fetch, FetchResponse, getItemPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getAppUser = async (id: string | string[] | undefined) =>
  id ? await fetch<AppUser>(`/app_users/${id}`) : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const { data: { data: appuser, hubURL, text } = { hubURL: null, text: "" } } =
    useQuery<FetchResponse<AppUser> | undefined>(["appuser", id], () =>
      getAppUser(id)
    );
  const appuserData = useMercure(appuser, hubURL);

  if (!appuserData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show AppUser ${appuserData["@id"]}`}</title>
        </Head>
      </div>
      <Show appuser={appuserData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["appuser", id], () => getAppUser(id));

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<AppUser>>("/app_users");
  const paths = await getItemPaths(response, "app_users", "/appusers/[id]");

  return {
    paths,
    fallback: true,
  };
};

export default Page;
