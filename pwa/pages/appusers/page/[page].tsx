import { GetStaticPaths, GetStaticProps } from "next";
import { dehydrate, QueryClient } from "react-query";

import {
  PageList,
  getAppUsers,
  getAppUsersPath,
} from "../../../components/appuser/PageList";
import { PagedCollection } from "../../../types/collection";
import { AppUser } from "../../../types/AppUser";
import { fetch, getCollectionPaths } from "../../../utils/dataAccess";

export const getStaticProps: GetStaticProps = async ({
  params: { page } = {},
}) => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(getAppUsersPath(page), getAppUsers(page));

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<AppUser>>("/app_users");
  const paths = await getCollectionPaths(
    response,
    "app_users",
    "/appusers/page/[page]"
  );

  return {
    paths,
    fallback: true,
  };
};

export default PageList;
