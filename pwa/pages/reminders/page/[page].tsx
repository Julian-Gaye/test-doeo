import { GetStaticPaths, GetStaticProps } from "next";
import { dehydrate, QueryClient } from "react-query";

import {
  PageList,
  getReminders,
  getRemindersPath,
} from "../../../components/reminder/PageList";
import { PagedCollection } from "../../../types/collection";
import { Reminder } from "../../../types/Reminder";
import { fetch, getCollectionPaths } from "../../../utils/dataAccess";

export const getStaticProps: GetStaticProps = async ({
  params: { page } = {},
}) => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(getRemindersPath(page), getReminders(page));

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<Reminder>>("/reminders");
  const paths = await getCollectionPaths(
    response,
    "reminders",
    "/reminders/page/[page]"
  );

  return {
    paths,
    fallback: true,
  };
};

export default PageList;
