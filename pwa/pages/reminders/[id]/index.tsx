import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/reminder/Show";
import { PagedCollection } from "../../../types/collection";
import { Reminder } from "../../../types/Reminder";
import { fetch, FetchResponse, getItemPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getReminder = async (id: string | string[] | undefined) =>
  id ? await fetch<Reminder>(`/reminders/${id}`) : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const {
    data: { data: reminder, hubURL, text } = { hubURL: null, text: "" },
  } = useQuery<FetchResponse<Reminder> | undefined>(["reminder", id], () =>
    getReminder(id)
  );
  const reminderData = useMercure(reminder, hubURL);

  if (!reminderData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show Reminder ${reminderData["@id"]}`}</title>
        </Head>
      </div>
      <Show reminder={reminderData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["reminder", id], () => getReminder(id));

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<Reminder>>("/reminders");
  const paths = await getItemPaths(response, "reminders", "/reminders/[id]");

  return {
    paths,
    fallback: true,
  };
};

export default Page;
