import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Form } from "../../../components/reminder/Form";
import { PagedCollection } from "../../../types/collection";
import { Reminder } from "../../../types/Reminder";
import { fetch, FetchResponse, getItemPaths } from "../../../utils/dataAccess";

const getReminder = async (id: string | string[] | undefined) =>
  id ? await fetch<Reminder>(`/reminders/${id}`) : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const { data: { data: reminder } = {} } = useQuery<
    FetchResponse<Reminder> | undefined
  >(["reminder", id], () => getReminder(id));

  if (!reminder) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{reminder && `Edit Reminder ${reminder["@id"]}`}</title>
        </Head>
      </div>
      <Form reminder={reminder} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["reminder", id], () => getReminder(id));

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<Reminder>>("/reminders");
  const paths = await getItemPaths(
    response,
    "reminders",
    "/reminders/[id]/edit"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
