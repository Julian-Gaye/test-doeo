import { NextComponentType, NextPageContext } from "next";
import Head from "next/head";

import { Form } from "../../components/organization/Form";

const Page: NextComponentType<NextPageContext> = () => (
  <div>
    <div>
      <Head>
        <title>Create Organization</title>
      </Head>
    </div>
    <Form />
  </div>
);

export default Page;
