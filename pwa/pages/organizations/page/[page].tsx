import { GetStaticPaths, GetStaticProps } from "next";
import { dehydrate, QueryClient } from "react-query";

import {
  PageList,
  getOrganizations,
  getOrganizationsPath,
} from "../../../components/organization/PageList";
import { PagedCollection } from "../../../types/collection";
import { Organization } from "../../../types/Organization";
import { fetch, getCollectionPaths } from "../../../utils/dataAccess";

export const getStaticProps: GetStaticProps = async ({
  params: { page } = {},
}) => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(
    getOrganizationsPath(page),
    getOrganizations(page)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<Organization>>("/organizations");
  const paths = await getCollectionPaths(
    response,
    "organizations",
    "/organizations/page/[page]"
  );

  return {
    paths,
    fallback: true,
  };
};

export default PageList;
