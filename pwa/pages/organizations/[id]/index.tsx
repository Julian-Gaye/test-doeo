import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Show } from "../../../components/organization/Show";
import { PagedCollection } from "../../../types/collection";
import { Organization } from "../../../types/Organization";
import { fetch, FetchResponse, getItemPaths } from "../../../utils/dataAccess";
import { useMercure } from "../../../utils/mercure";

const getOrganization = async (id: string | string[] | undefined) =>
  id
    ? await fetch<Organization>(`/organizations/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const {
    data: { data: organization, hubURL, text } = { hubURL: null, text: "" },
  } = useQuery<FetchResponse<Organization> | undefined>(
    ["organization", id],
    () => getOrganization(id)
  );
  const organizationData = useMercure(organization, hubURL);

  if (!organizationData) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{`Show Organization ${organizationData["@id"]}`}</title>
        </Head>
      </div>
      <Show organization={organizationData} text={text} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["organization", id], () =>
    getOrganization(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<Organization>>("/organizations");
  const paths = await getItemPaths(
    response,
    "organizations",
    "/organizations/[id]"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
