import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Form } from "../../../components/organization/Form";
import { PagedCollection } from "../../../types/collection";
import { Organization } from "../../../types/Organization";
import { fetch, FetchResponse, getItemPaths } from "../../../utils/dataAccess";

const getOrganization = async (id: string | string[] | undefined) =>
  id
    ? await fetch<Organization>(`/organizations/${id}`)
    : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const { data: { data: organization } = {} } = useQuery<
    FetchResponse<Organization> | undefined
  >(["organization", id], () => getOrganization(id));

  if (!organization) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>
            {organization && `Edit Organization ${organization["@id"]}`}
          </title>
        </Head>
      </div>
      <Form organization={organization} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["organization", id], () =>
    getOrganization(id)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<Organization>>("/organizations");
  const paths = await getItemPaths(
    response,
    "organizations",
    "/organizations/[id]/edit"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
