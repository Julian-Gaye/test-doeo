import { GetStaticPaths, GetStaticProps } from "next";
import { dehydrate, QueryClient } from "react-query";

import {
  PageList,
  getUserDoeos,
  getUserDoeosPath,
} from "../../../components/userdoeo/PageList";
import { PagedCollection } from "../../../types/collection";
import { UserDoeo } from "../../../types/UserDoeo";
import { fetch, getCollectionPaths } from "../../../utils/dataAccess";

export const getStaticProps: GetStaticProps = async ({
  params: { page } = {},
}) => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(getUserDoeosPath(page), getUserDoeos(page));

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<UserDoeo>>("/user_doeos");
  const paths = await getCollectionPaths(
    response,
    "user_doeos",
    "/userdoeos/page/[page]"
  );

  return {
    paths,
    fallback: true,
  };
};

export default PageList;
