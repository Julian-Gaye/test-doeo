import { NextComponentType, NextPageContext } from "next";
import Head from "next/head";

import { Form } from "../../components/userdoeo/Form";

const Page: NextComponentType<NextPageContext> = () => (
  <div>
    <div>
      <Head>
        <title>Create UserDoeo</title>
      </Head>
    </div>
    <Form />
  </div>
);

export default Page;
