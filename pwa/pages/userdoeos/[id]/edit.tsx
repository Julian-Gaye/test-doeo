import {
  GetStaticPaths,
  GetStaticProps,
  NextComponentType,
  NextPageContext,
} from "next";
import DefaultErrorPage from "next/error";
import Head from "next/head";
import { useRouter } from "next/router";
import { dehydrate, QueryClient, useQuery } from "react-query";

import { Form } from "../../../components/userdoeo/Form";
import { PagedCollection } from "../../../types/collection";
import { UserDoeo } from "../../../types/UserDoeo";
import { fetch, FetchResponse, getItemPaths } from "../../../utils/dataAccess";

const getUserDoeo = async (id: string | string[] | undefined) =>
  id ? await fetch<UserDoeo>(`/user_doeos/${id}`) : Promise.resolve(undefined);

const Page: NextComponentType<NextPageContext> = () => {
  const router = useRouter();
  const { id } = router.query;

  const { data: { data: userdoeo } = {} } = useQuery<
    FetchResponse<UserDoeo> | undefined
  >(["userdoeo", id], () => getUserDoeo(id));

  if (!userdoeo) {
    return <DefaultErrorPage statusCode={404} />;
  }

  return (
    <div>
      <div>
        <Head>
          <title>{userdoeo && `Edit UserDoeo ${userdoeo["@id"]}`}</title>
        </Head>
      </div>
      <Form userdoeo={userdoeo} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async ({
  params: { id } = {},
}) => {
  if (!id) throw new Error("id not in query param");
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(["userdoeo", id], () => getUserDoeo(id));

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
    revalidate: 1,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch<PagedCollection<UserDoeo>>("/user_doeos");
  const paths = await getItemPaths(
    response,
    "user_doeos",
    "/userdoeos/[id]/edit"
  );

  return {
    paths,
    fallback: true,
  };
};

export default Page;
