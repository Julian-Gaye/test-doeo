import { NextComponentType, NextPageContext } from "next";
import { useRouter } from "next/router";
import Head from "next/head";
import { useQuery } from "react-query";

import Pagination from "../common/Pagination";
import { List } from "./List";
import { PagedCollection } from "../../types/collection";
import { UserDoeo } from "../../types/UserDoeo";
import { fetch, FetchResponse, parsePage } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

export const getUserDoeosPath = (page?: string | string[] | undefined) =>
  `/user_doeos${typeof page === "string" ? `?page=${page}` : ""}`;
export const getUserDoeos =
  (page?: string | string[] | undefined) => async () =>
    await fetch<PagedCollection<UserDoeo>>(getUserDoeosPath(page));
const getPagePath = (path: string) =>
  `/userdoeos/page/${parsePage("user_doeos", path)}`;

export const PageList: NextComponentType<NextPageContext> = () => {
  const {
    query: { page },
  } = useRouter();
  const { data: { data: userdoeos, hubURL } = { hubURL: null } } = useQuery<
    FetchResponse<PagedCollection<UserDoeo>> | undefined
  >(getUserDoeosPath(page), getUserDoeos(page));
  const collection = useMercure(userdoeos, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>UserDoeo List</title>
        </Head>
      </div>
      <List userdoeos={collection["hydra:member"]} />
      <Pagination collection={collection} getPagePath={getPagePath} />
    </div>
  );
};
