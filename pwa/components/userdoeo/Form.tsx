import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Field, FieldArray, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { UserDoeo } from "../../types/UserDoeo";

interface Props {
  userdoeo?: UserDoeo;
}

interface SaveParams {
  values: UserDoeo;
}

interface DeleteParams {
  id: string;
}

const saveUserDoeo = async ({ values }: SaveParams) =>
  await fetch<UserDoeo>(!values["@id"] ? "/user_doeos" : values["@id"], {
    method: !values["@id"] ? "POST" : "PUT",
    body: JSON.stringify(values),
  });

const deleteUserDoeo = async (id: string) =>
  await fetch<UserDoeo>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ userdoeo }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<UserDoeo> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveUserDoeo(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<UserDoeo> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteUserDoeo(id), {
    onSuccess: () => {
      router.push("/userdoeos");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!userdoeo || !userdoeo["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: userdoeo["@id"] });
  };

  return (
    <div className="container mx-auto px-4 max-w-2xl mt-4">
      <Link
        href="/userdoeos"
        className="text-sm text-cyan-500 font-bold hover:text-cyan-700"
      >
        {`< Back to list`}
      </Link>
      <h1 className="text-3xl my-2">
        {userdoeo ? `Edit UserDoeo ${userdoeo["@id"]}` : `Create UserDoeo`}
      </h1>
      <Formik
        initialValues={
          userdoeo
            ? {
                ...userdoeo,
              }
            : new UserDoeo()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/user_doeos");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form className="shadow-md p-4" onSubmit={handleSubmit}>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="userdoeo_username"
              >
                username
              </label>
              <input
                name="username"
                id="userdoeo_username"
                value={values.username ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.username && touched.username ? "border-red-500" : ""
                }`}
                aria-invalid={
                  errors.username && touched.username ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="username"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="userdoeo_fullname"
              >
                fullname
              </label>
              <input
                name="fullname"
                id="userdoeo_fullname"
                value={values.fullname ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.fullname && touched.fullname ? "border-red-500" : ""
                }`}
                aria-invalid={
                  errors.fullname && touched.fullname ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="fullname"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="userdoeo_email"
              >
                email
              </label>
              <input
                name="email"
                id="userdoeo_email"
                value={values.email ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.email && touched.email ? "border-red-500" : ""
                }`}
                aria-invalid={
                  errors.email && touched.email ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="email"
              />
            </div>
            <div className="mb-2">
              <div className="text-gray-700 block text-sm font-bold">talks</div>
              <FieldArray
                name="talks"
                render={(arrayHelpers) => (
                  <div className="mb-2" id="userdoeo_talks">
                    {values.talks && values.talks.length > 0 ? (
                      values.talks.map((item: any, index: number) => (
                        <div key={index}>
                          <Field name={`talks.${index}`} />
                          <button
                            type="button"
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            -
                          </button>
                          <button
                            type="button"
                            onClick={() => arrayHelpers.insert(index, "")}
                          >
                            +
                          </button>
                        </div>
                      ))
                    ) : (
                      <button
                        type="button"
                        onClick={() => arrayHelpers.push("")}
                      >
                        Add
                      </button>
                    )}
                  </div>
                )}
              />
            </div>
            <div className="mb-2">
              <div className="text-gray-700 block text-sm font-bold">
                reminders
              </div>
              <FieldArray
                name="reminders"
                render={(arrayHelpers) => (
                  <div className="mb-2" id="userdoeo_reminders">
                    {values.reminders && values.reminders.length > 0 ? (
                      values.reminders.map((item: any, index: number) => (
                        <div key={index}>
                          <Field name={`reminders.${index}`} />
                          <button
                            type="button"
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            -
                          </button>
                          <button
                            type="button"
                            onClick={() => arrayHelpers.insert(index, "")}
                          >
                            +
                          </button>
                        </div>
                      ))
                    ) : (
                      <button
                        type="button"
                        onClick={() => arrayHelpers.push("")}
                      >
                        Add
                      </button>
                    )}
                  </div>
                )}
              />
            </div>
            {status && status.msg && (
              <div
                className={`border px-4 py-3 my-4 rounded ${
                  status.isValid
                    ? "text-cyan-700 border-cyan-500 bg-cyan-200/50"
                    : "text-red-700 border-red-400 bg-red-100"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="inline-block mt-2 bg-cyan-500 hover:bg-cyan-700 text-sm text-white font-bold py-2 px-4 rounded"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <div className="flex space-x-2 mt-4 justify-end">
        {userdoeo && (
          <button
            className="inline-block mt-2 border-2 border-red-400 hover:border-red-700 hover:text-red-700 text-sm text-red-400 font-bold py-2 px-4 rounded"
            onClick={handleDelete}
          >
            Delete
          </button>
        )}
      </div>
    </div>
  );
};
