import { NextComponentType, NextPageContext } from "next";
import { useRouter } from "next/router";
import Head from "next/head";
import { useQuery } from "react-query";

import Pagination from "../common/Pagination";
import { List } from "./List";
import { PagedCollection } from "../../types/collection";
import { Talk } from "../../types/Talk";
import { fetch, FetchResponse, parsePage } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

export const getTalksPath = (page?: string | string[] | undefined) =>
  `/talks${typeof page === "string" ? `?page=${page}` : ""}`;
export const getTalks = (page?: string | string[] | undefined) => async () =>
  await fetch<PagedCollection<Talk>>(getTalksPath(page));
const getPagePath = (path: string) => `/talks/page/${parsePage("talks", path)}`;

export const PageList: NextComponentType<NextPageContext> = () => {
  const {
    query: { page },
  } = useRouter();
  const { data: { data: talks, hubURL } = { hubURL: null } } = useQuery<
    FetchResponse<PagedCollection<Talk>> | undefined
  >(getTalksPath(page), getTalks(page));
  const collection = useMercure(talks, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>Talk List</title>
        </Head>
      </div>
      <List talks={collection["hydra:member"]} />
      <Pagination collection={collection} getPagePath={getPagePath} />
    </div>
  );
};
