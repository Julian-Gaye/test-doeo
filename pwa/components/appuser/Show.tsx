import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import Head from "next/head";

import ReferenceLinks from "../common/ReferenceLinks";
import { fetch, getItemPath } from "../../utils/dataAccess";
import { AppUser } from "../../types/AppUser";

interface Props {
  appuser: AppUser;
  text: string;
}

export const Show: FunctionComponent<Props> = ({ appuser, text }) => {
  const [error, setError] = useState<string | null>(null);
  const router = useRouter();

  const handleDelete = async () => {
    if (!appuser["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;

    try {
      await fetch(appuser["@id"], { method: "DELETE" });
      router.push("/appusers");
    } catch (error) {
      setError("Error when deleting the resource.");
      console.error(error);
    }
  };

  return (
    <div className="p-4">
      <Head>
        <title>{`Show AppUser ${appuser["@id"]}`}</title>
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{ __html: text }}
        />
      </Head>
      <Link
        href="/appusers"
        className="text-sm text-cyan-500 font-bold hover:text-cyan-700"
      >
        {"< Back to list"}
      </Link>
      <h1 className="text-3xl mb-2">{`Show AppUser ${appuser["@id"]}`}</h1>
      <table
        cellPadding={10}
        className="shadow-md table border-collapse min-w-full leading-normal table-auto text-left my-3"
      >
        <thead className="w-full text-xs uppercase font-light text-gray-700 bg-gray-200 py-2 px-4">
          <tr>
            <th>Field</th>
            <th>Value</th>
          </tr>
        </thead>
        <tbody className="text-sm divide-y divide-gray-200">
          <tr>
            <th scope="row">last_name</th>
            <td>{appuser["last_name"]}</td>
          </tr>
          <tr>
            <th scope="row">first_name</th>
            <td>{appuser["first_name"]}</td>
          </tr>
          <tr>
            <th scope="row">status</th>
            <td>{appuser["status"]}</td>
          </tr>
          <tr>
            <th scope="row">last_login</th>
            <td>{appuser["last_login"]?.toLocaleString()}</td>
          </tr>
          <tr>
            <th scope="row">created_at</th>
            <td>{appuser["created_at"]?.toLocaleString()}</td>
          </tr>
          <tr>
            <th scope="row">deleted_at</th>
            <td>{appuser["deleted_at"]?.toLocaleString()}</td>
          </tr>
          <tr>
            <th scope="row">organization</th>
            <td>
              <ReferenceLinks
                items={appuser["organization"].map((ref: any) => ({
                  href: getItemPath(ref, "/organizations/[id]"),
                  name: ref,
                }))}
              />
            </td>
          </tr>
          <tr>
            <th scope="row">lastName</th>
            <td>{appuser["lastName"]}</td>
          </tr>
          <tr>
            <th scope="row">firstName</th>
            <td>{appuser["firstName"]}</td>
          </tr>
          <tr>
            <th scope="row">lastLogin</th>
            <td>{appuser["lastLogin"]?.toLocaleString()}</td>
          </tr>
          <tr>
            <th scope="row">createdAt</th>
            <td>{appuser["createdAt"]?.toLocaleString()}</td>
          </tr>
          <tr>
            <th scope="row">deletedAt</th>
            <td>{appuser["deletedAt"]?.toLocaleString()}</td>
          </tr>
        </tbody>
      </table>
      {error && (
        <div
          className="border px-4 py-3 my-4 rounded text-red-700 border-red-400 bg-red-100"
          role="alert"
        >
          {error}
        </div>
      )}
      <div className="flex space-x-2 mt-4 items-center justify-end">
        <Link
          href={getItemPath(appuser["@id"], "/appusers/[id]/edit")}
          className="inline-block mt-2 border-2 border-cyan-500 bg-cyan-500 hover:border-cyan-700 hover:bg-cyan-700 text-xs text-white font-bold py-2 px-4 rounded"
        >
          Edit
        </Link>
        <button
          className="inline-block mt-2 border-2 border-red-400 hover:border-red-700 hover:text-red-700 text-xs text-red-400 font-bold py-2 px-4 rounded"
          onClick={handleDelete}
        >
          Delete
        </button>
      </div>
    </div>
  );
};
