import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Field, FieldArray, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { AppUser } from "../../types/AppUser";

interface Props {
  appuser?: AppUser;
}

interface SaveParams {
  values: AppUser;
}

interface DeleteParams {
  id: string;
}

const saveAppUser = async ({ values }: SaveParams) =>
  await fetch<AppUser>(!values["@id"] ? "/app_users" : values["@id"], {
    method: !values["@id"] ? "POST" : "PUT",
    body: JSON.stringify(values),
  });

const deleteAppUser = async (id: string) =>
  await fetch<AppUser>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ appuser }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<AppUser> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveAppUser(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<AppUser> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteAppUser(id), {
    onSuccess: () => {
      router.push("/appusers");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!appuser || !appuser["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: appuser["@id"] });
  };

  return (
    <div className="container mx-auto px-4 max-w-2xl mt-4">
      <Link
        href="/appusers"
        className="text-sm text-cyan-500 font-bold hover:text-cyan-700"
      >
        {`< Back to list`}
      </Link>
      <h1 className="text-3xl my-2">
        {appuser ? `Edit AppUser ${appuser["@id"]}` : `Create AppUser`}
      </h1>
      <Formik
        initialValues={
          appuser
            ? {
                ...appuser,
              }
            : new AppUser()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/app_users");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form className="shadow-md p-4" onSubmit={handleSubmit}>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="appuser_last_name"
              >
                last_name
              </label>
              <input
                name="last_name"
                id="appuser_last_name"
                value={values.last_name ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.last_name && touched.last_name ? "border-red-500" : ""
                }`}
                aria-invalid={
                  errors.last_name && touched.last_name ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="last_name"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="appuser_first_name"
              >
                first_name
              </label>
              <input
                name="first_name"
                id="appuser_first_name"
                value={values.first_name ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.first_name && touched.first_name
                    ? "border-red-500"
                    : ""
                }`}
                aria-invalid={
                  errors.first_name && touched.first_name ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="first_name"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="appuser_status"
              >
                status
              </label>
              <input
                name="status"
                id="appuser_status"
                value={values.status ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.status && touched.status ? "border-red-500" : ""
                }`}
                aria-invalid={
                  errors.status && touched.status ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="status"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="appuser_last_login"
              >
                last_login
              </label>
              <input
                name="last_login"
                id="appuser_last_login"
                value={values.last_login?.toLocaleString() ?? ""}
                type="dateTime"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.last_login && touched.last_login
                    ? "border-red-500"
                    : ""
                }`}
                aria-invalid={
                  errors.last_login && touched.last_login ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="last_login"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="appuser_created_at"
              >
                created_at
              </label>
              <input
                name="created_at"
                id="appuser_created_at"
                value={values.created_at?.toLocaleString() ?? ""}
                type="dateTime"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.created_at && touched.created_at
                    ? "border-red-500"
                    : ""
                }`}
                aria-invalid={
                  errors.created_at && touched.created_at ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="created_at"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="appuser_deleted_at"
              >
                deleted_at
              </label>
              <input
                name="deleted_at"
                id="appuser_deleted_at"
                value={values.deleted_at?.toLocaleString() ?? ""}
                type="dateTime"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.deleted_at && touched.deleted_at
                    ? "border-red-500"
                    : ""
                }`}
                aria-invalid={
                  errors.deleted_at && touched.deleted_at ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="deleted_at"
              />
            </div>
            <div className="mb-2">
              <div className="text-gray-700 block text-sm font-bold">
                organization
              </div>
              <FieldArray
                name="organization"
                render={(arrayHelpers) => (
                  <div className="mb-2" id="appuser_organization">
                    {values.organization && values.organization.length > 0 ? (
                      values.organization.map((item: any, index: number) => (
                        <div key={index}>
                          <Field name={`organization.${index}`} />
                          <button
                            type="button"
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            -
                          </button>
                          <button
                            type="button"
                            onClick={() => arrayHelpers.insert(index, "")}
                          >
                            +
                          </button>
                        </div>
                      ))
                    ) : (
                      <button
                        type="button"
                        onClick={() => arrayHelpers.push("")}
                      >
                        Add
                      </button>
                    )}
                  </div>
                )}
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="appuser_lastName"
              >
                lastName
              </label>
              <input
                name="lastName"
                id="appuser_lastName"
                value={values.lastName ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.lastName && touched.lastName ? "border-red-500" : ""
                }`}
                aria-invalid={
                  errors.lastName && touched.lastName ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="lastName"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="appuser_firstName"
              >
                firstName
              </label>
              <input
                name="firstName"
                id="appuser_firstName"
                value={values.firstName ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.firstName && touched.firstName ? "border-red-500" : ""
                }`}
                aria-invalid={
                  errors.firstName && touched.firstName ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="firstName"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="appuser_lastLogin"
              >
                lastLogin
              </label>
              <input
                name="lastLogin"
                id="appuser_lastLogin"
                value={values.lastLogin?.toLocaleString() ?? ""}
                type="dateTime"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.lastLogin && touched.lastLogin ? "border-red-500" : ""
                }`}
                aria-invalid={
                  errors.lastLogin && touched.lastLogin ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="lastLogin"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="appuser_createdAt"
              >
                createdAt
              </label>
              <input
                name="createdAt"
                id="appuser_createdAt"
                value={values.createdAt?.toLocaleString() ?? ""}
                type="dateTime"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.createdAt && touched.createdAt ? "border-red-500" : ""
                }`}
                aria-invalid={
                  errors.createdAt && touched.createdAt ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="createdAt"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="appuser_deletedAt"
              >
                deletedAt
              </label>
              <input
                name="deletedAt"
                id="appuser_deletedAt"
                value={values.deletedAt?.toLocaleString() ?? ""}
                type="dateTime"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.deletedAt && touched.deletedAt ? "border-red-500" : ""
                }`}
                aria-invalid={
                  errors.deletedAt && touched.deletedAt ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="deletedAt"
              />
            </div>
            {status && status.msg && (
              <div
                className={`border px-4 py-3 my-4 rounded ${
                  status.isValid
                    ? "text-cyan-700 border-cyan-500 bg-cyan-200/50"
                    : "text-red-700 border-red-400 bg-red-100"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="inline-block mt-2 bg-cyan-500 hover:bg-cyan-700 text-sm text-white font-bold py-2 px-4 rounded"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <div className="flex space-x-2 mt-4 justify-end">
        {appuser && (
          <button
            className="inline-block mt-2 border-2 border-red-400 hover:border-red-700 hover:text-red-700 text-sm text-red-400 font-bold py-2 px-4 rounded"
            onClick={handleDelete}
          >
            Delete
          </button>
        )}
      </div>
    </div>
  );
};
