import { NextComponentType, NextPageContext } from "next";
import { useRouter } from "next/router";
import Head from "next/head";
import { useQuery } from "react-query";

import Pagination from "../common/Pagination";
import { List } from "./List";
import { PagedCollection } from "../../types/collection";
import { Organization } from "../../types/Organization";
import { fetch, FetchResponse, parsePage } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

export const getOrganizationsPath = (page?: string | string[] | undefined) =>
  `/organizations${typeof page === "string" ? `?page=${page}` : ""}`;
export const getOrganizations =
  (page?: string | string[] | undefined) => async () =>
    await fetch<PagedCollection<Organization>>(getOrganizationsPath(page));
const getPagePath = (path: string) =>
  `/organizations/page/${parsePage("organizations", path)}`;

export const PageList: NextComponentType<NextPageContext> = () => {
  const {
    query: { page },
  } = useRouter();
  const { data: { data: organizations, hubURL } = { hubURL: null } } = useQuery<
    FetchResponse<PagedCollection<Organization>> | undefined
  >(getOrganizationsPath(page), getOrganizations(page));
  const collection = useMercure(organizations, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>Organization List</title>
        </Head>
      </div>
      <List organizations={collection["hydra:member"]} />
      <Pagination collection={collection} getPagePath={getPagePath} />
    </div>
  );
};
