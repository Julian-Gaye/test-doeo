import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import Head from "next/head";

import ReferenceLinks from "../common/ReferenceLinks";
import { fetch, getItemPath } from "../../utils/dataAccess";
import { Organization } from "../../types/Organization";

interface Props {
  organization: Organization;
  text: string;
}

export const Show: FunctionComponent<Props> = ({ organization, text }) => {
  const [error, setError] = useState<string | null>(null);
  const router = useRouter();

  const handleDelete = async () => {
    if (!organization["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;

    try {
      await fetch(organization["@id"], { method: "DELETE" });
      router.push("/organizations");
    } catch (error) {
      setError("Error when deleting the resource.");
      console.error(error);
    }
  };

  return (
    <div className="p-4">
      <Head>
        <title>{`Show Organization ${organization["@id"]}`}</title>
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{ __html: text }}
        />
      </Head>
      <Link
        href="/organizations"
        className="text-sm text-cyan-500 font-bold hover:text-cyan-700"
      >
        {"< Back to list"}
      </Link>
      <h1 className="text-3xl mb-2">{`Show Organization ${organization["@id"]}`}</h1>
      <table
        cellPadding={10}
        className="shadow-md table border-collapse min-w-full leading-normal table-auto text-left my-3"
      >
        <thead className="w-full text-xs uppercase font-light text-gray-700 bg-gray-200 py-2 px-4">
          <tr>
            <th>Field</th>
            <th>Value</th>
          </tr>
        </thead>
        <tbody className="text-sm divide-y divide-gray-200">
          <tr>
            <th scope="row">name</th>
            <td>{organization["name"]}</td>
          </tr>
          <tr>
            <th scope="row">status</th>
            <td>{organization["status"]}</td>
          </tr>
          <tr>
            <th scope="row">referent_name</th>
            <td>{organization["referent_name"]}</td>
          </tr>
          <tr>
            <th scope="row">referent_function</th>
            <td>{organization["referent_function"]}</td>
          </tr>
          <tr>
            <th scope="row">referent_mail</th>
            <td>{organization["referent_mail"]}</td>
          </tr>
          <tr>
            <th scope="row">referent_phone</th>
            <td>{organization["referent_phone"]}</td>
          </tr>
          <tr>
            <th scope="row">talks</th>
            <td>
              <ReferenceLinks
                items={organization["talks"].map((ref: any) => ({
                  href: getItemPath(ref, "/talks/[id]"),
                  name: ref,
                }))}
              />
            </td>
          </tr>
          <tr>
            <th scope="row">reminders</th>
            <td>
              <ReferenceLinks
                items={organization["reminders"].map((ref: any) => ({
                  href: getItemPath(ref, "/reminders/[id]"),
                  name: ref,
                }))}
              />
            </td>
          </tr>
          <tr>
            <th scope="row">appUsers</th>
            <td>
              <ReferenceLinks
                items={organization["appUsers"].map((ref: any) => ({
                  href: getItemPath(ref, "/appusers/[id]"),
                  name: ref,
                }))}
              />
            </td>
          </tr>
          <tr>
            <th scope="row">referentName</th>
            <td>{organization["referentName"]}</td>
          </tr>
          <tr>
            <th scope="row">referentFunction</th>
            <td>{organization["referentFunction"]}</td>
          </tr>
          <tr>
            <th scope="row">referentMail</th>
            <td>{organization["referentMail"]}</td>
          </tr>
          <tr>
            <th scope="row">referentPhone</th>
            <td>{organization["referentPhone"]}</td>
          </tr>
        </tbody>
      </table>
      {error && (
        <div
          className="border px-4 py-3 my-4 rounded text-red-700 border-red-400 bg-red-100"
          role="alert"
        >
          {error}
        </div>
      )}
      <div className="flex space-x-2 mt-4 items-center justify-end">
        <Link
          href={getItemPath(organization["@id"], "/organizations/[id]/edit")}
          className="inline-block mt-2 border-2 border-cyan-500 bg-cyan-500 hover:border-cyan-700 hover:bg-cyan-700 text-xs text-white font-bold py-2 px-4 rounded"
        >
          Edit
        </Link>
        <button
          className="inline-block mt-2 border-2 border-red-400 hover:border-red-700 hover:text-red-700 text-xs text-red-400 font-bold py-2 px-4 rounded"
          onClick={handleDelete}
        >
          Delete
        </button>
      </div>
    </div>
  );
};
