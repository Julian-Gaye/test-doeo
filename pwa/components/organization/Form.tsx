import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Field, FieldArray, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { Organization } from "../../types/Organization";

interface Props {
  organization?: Organization;
}

interface SaveParams {
  values: Organization;
}

interface DeleteParams {
  id: string;
}

const saveOrganization = async ({ values }: SaveParams) =>
  await fetch<Organization>(!values["@id"] ? "/organizations" : values["@id"], {
    method: !values["@id"] ? "POST" : "PUT",
    body: JSON.stringify(values),
  });

const deleteOrganization = async (id: string) =>
  await fetch<Organization>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ organization }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<Organization> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveOrganization(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<Organization> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteOrganization(id), {
    onSuccess: () => {
      router.push("/organizations");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!organization || !organization["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: organization["@id"] });
  };

  return (
    <div className="container mx-auto px-4 max-w-2xl mt-4">
      <Link
        href="/organizations"
        className="text-sm text-cyan-500 font-bold hover:text-cyan-700"
      >
        {`< Back to list`}
      </Link>
      <h1 className="text-3xl my-2">
        {organization
          ? `Edit Organization ${organization["@id"]}`
          : `Create Organization`}
      </h1>
      <Formik
        initialValues={
          organization
            ? {
                ...organization,
              }
            : new Organization()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/organizations");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form className="shadow-md p-4" onSubmit={handleSubmit}>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="organization_name"
              >
                name
              </label>
              <input
                name="name"
                id="organization_name"
                value={values.name ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.name && touched.name ? "border-red-500" : ""
                }`}
                aria-invalid={errors.name && touched.name ? "true" : undefined}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="name"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="organization_status"
              >
                status
              </label>
              <input
                name="status"
                id="organization_status"
                value={values.status ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.status && touched.status ? "border-red-500" : ""
                }`}
                aria-invalid={
                  errors.status && touched.status ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="status"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="organization_referent_name"
              >
                referent_name
              </label>
              <input
                name="referent_name"
                id="organization_referent_name"
                value={values.referent_name ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.referent_name && touched.referent_name
                    ? "border-red-500"
                    : ""
                }`}
                aria-invalid={
                  errors.referent_name && touched.referent_name
                    ? "true"
                    : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="referent_name"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="organization_referent_function"
              >
                referent_function
              </label>
              <input
                name="referent_function"
                id="organization_referent_function"
                value={values.referent_function ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.referent_function && touched.referent_function
                    ? "border-red-500"
                    : ""
                }`}
                aria-invalid={
                  errors.referent_function && touched.referent_function
                    ? "true"
                    : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="referent_function"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="organization_referent_mail"
              >
                referent_mail
              </label>
              <input
                name="referent_mail"
                id="organization_referent_mail"
                value={values.referent_mail ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.referent_mail && touched.referent_mail
                    ? "border-red-500"
                    : ""
                }`}
                aria-invalid={
                  errors.referent_mail && touched.referent_mail
                    ? "true"
                    : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="referent_mail"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="organization_referent_phone"
              >
                referent_phone
              </label>
              <input
                name="referent_phone"
                id="organization_referent_phone"
                value={values.referent_phone ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.referent_phone && touched.referent_phone
                    ? "border-red-500"
                    : ""
                }`}
                aria-invalid={
                  errors.referent_phone && touched.referent_phone
                    ? "true"
                    : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="referent_phone"
              />
            </div>
            <div className="mb-2">
              <div className="text-gray-700 block text-sm font-bold">talks</div>
              <FieldArray
                name="talks"
                render={(arrayHelpers) => (
                  <div className="mb-2" id="organization_talks">
                    {values.talks && values.talks.length > 0 ? (
                      values.talks.map((item: any, index: number) => (
                        <div key={index}>
                          <Field name={`talks.${index}`} />
                          <button
                            type="button"
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            -
                          </button>
                          <button
                            type="button"
                            onClick={() => arrayHelpers.insert(index, "")}
                          >
                            +
                          </button>
                        </div>
                      ))
                    ) : (
                      <button
                        type="button"
                        onClick={() => arrayHelpers.push("")}
                      >
                        Add
                      </button>
                    )}
                  </div>
                )}
              />
            </div>
            <div className="mb-2">
              <div className="text-gray-700 block text-sm font-bold">
                reminders
              </div>
              <FieldArray
                name="reminders"
                render={(arrayHelpers) => (
                  <div className="mb-2" id="organization_reminders">
                    {values.reminders && values.reminders.length > 0 ? (
                      values.reminders.map((item: any, index: number) => (
                        <div key={index}>
                          <Field name={`reminders.${index}`} />
                          <button
                            type="button"
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            -
                          </button>
                          <button
                            type="button"
                            onClick={() => arrayHelpers.insert(index, "")}
                          >
                            +
                          </button>
                        </div>
                      ))
                    ) : (
                      <button
                        type="button"
                        onClick={() => arrayHelpers.push("")}
                      >
                        Add
                      </button>
                    )}
                  </div>
                )}
              />
            </div>
            <div className="mb-2">
              <div className="text-gray-700 block text-sm font-bold">
                appUsers
              </div>
              <FieldArray
                name="appUsers"
                render={(arrayHelpers) => (
                  <div className="mb-2" id="organization_appUsers">
                    {values.appUsers && values.appUsers.length > 0 ? (
                      values.appUsers.map((item: any, index: number) => (
                        <div key={index}>
                          <Field name={`appUsers.${index}`} />
                          <button
                            type="button"
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            -
                          </button>
                          <button
                            type="button"
                            onClick={() => arrayHelpers.insert(index, "")}
                          >
                            +
                          </button>
                        </div>
                      ))
                    ) : (
                      <button
                        type="button"
                        onClick={() => arrayHelpers.push("")}
                      >
                        Add
                      </button>
                    )}
                  </div>
                )}
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="organization_referentName"
              >
                referentName
              </label>
              <input
                name="referentName"
                id="organization_referentName"
                value={values.referentName ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.referentName && touched.referentName
                    ? "border-red-500"
                    : ""
                }`}
                aria-invalid={
                  errors.referentName && touched.referentName
                    ? "true"
                    : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="referentName"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="organization_referentFunction"
              >
                referentFunction
              </label>
              <input
                name="referentFunction"
                id="organization_referentFunction"
                value={values.referentFunction ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.referentFunction && touched.referentFunction
                    ? "border-red-500"
                    : ""
                }`}
                aria-invalid={
                  errors.referentFunction && touched.referentFunction
                    ? "true"
                    : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="referentFunction"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="organization_referentMail"
              >
                referentMail
              </label>
              <input
                name="referentMail"
                id="organization_referentMail"
                value={values.referentMail ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.referentMail && touched.referentMail
                    ? "border-red-500"
                    : ""
                }`}
                aria-invalid={
                  errors.referentMail && touched.referentMail
                    ? "true"
                    : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="referentMail"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="organization_referentPhone"
              >
                referentPhone
              </label>
              <input
                name="referentPhone"
                id="organization_referentPhone"
                value={values.referentPhone ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.referentPhone && touched.referentPhone
                    ? "border-red-500"
                    : ""
                }`}
                aria-invalid={
                  errors.referentPhone && touched.referentPhone
                    ? "true"
                    : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="referentPhone"
              />
            </div>
            {status && status.msg && (
              <div
                className={`border px-4 py-3 my-4 rounded ${
                  status.isValid
                    ? "text-cyan-700 border-cyan-500 bg-cyan-200/50"
                    : "text-red-700 border-red-400 bg-red-100"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="inline-block mt-2 bg-cyan-500 hover:bg-cyan-700 text-sm text-white font-bold py-2 px-4 rounded"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <div className="flex space-x-2 mt-4 justify-end">
        {organization && (
          <button
            className="inline-block mt-2 border-2 border-red-400 hover:border-red-700 hover:text-red-700 text-sm text-red-400 font-bold py-2 px-4 rounded"
            onClick={handleDelete}
          >
            Delete
          </button>
        )}
      </div>
    </div>
  );
};
