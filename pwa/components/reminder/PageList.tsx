import { NextComponentType, NextPageContext } from "next";
import { useRouter } from "next/router";
import Head from "next/head";
import { useQuery } from "react-query";

import Pagination from "../common/Pagination";
import { List } from "./List";
import { PagedCollection } from "../../types/collection";
import { Reminder } from "../../types/Reminder";
import { fetch, FetchResponse, parsePage } from "../../utils/dataAccess";
import { useMercure } from "../../utils/mercure";

export const getRemindersPath = (page?: string | string[] | undefined) =>
  `/reminders${typeof page === "string" ? `?page=${page}` : ""}`;
export const getReminders =
  (page?: string | string[] | undefined) => async () =>
    await fetch<PagedCollection<Reminder>>(getRemindersPath(page));
const getPagePath = (path: string) =>
  `/reminders/page/${parsePage("reminders", path)}`;

export const PageList: NextComponentType<NextPageContext> = () => {
  const {
    query: { page },
  } = useRouter();
  const { data: { data: reminders, hubURL } = { hubURL: null } } = useQuery<
    FetchResponse<PagedCollection<Reminder>> | undefined
  >(getRemindersPath(page), getReminders(page));
  const collection = useMercure(reminders, hubURL);

  if (!collection || !collection["hydra:member"]) return null;

  return (
    <div>
      <div>
        <Head>
          <title>Reminder List</title>
        </Head>
      </div>
      <List reminders={collection["hydra:member"]} />
      <Pagination collection={collection} getPagePath={getPagePath} />
    </div>
  );
};
