import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { ErrorMessage, Field, FieldArray, Formik } from "formik";
import { useMutation } from "react-query";

import { fetch, FetchError, FetchResponse } from "../../utils/dataAccess";
import { Reminder } from "../../types/Reminder";

interface Props {
  reminder?: Reminder;
}

interface SaveParams {
  values: Reminder;
}

interface DeleteParams {
  id: string;
}

const saveReminder = async ({ values }: SaveParams) =>
  await fetch<Reminder>(!values["@id"] ? "/reminders" : values["@id"], {
    method: !values["@id"] ? "POST" : "PUT",
    body: JSON.stringify(values),
  });

const deleteReminder = async (id: string) =>
  await fetch<Reminder>(id, { method: "DELETE" });

export const Form: FunctionComponent<Props> = ({ reminder }) => {
  const [, setError] = useState<string | null>(null);
  const router = useRouter();

  const saveMutation = useMutation<
    FetchResponse<Reminder> | undefined,
    Error | FetchError,
    SaveParams
  >((saveParams) => saveReminder(saveParams));

  const deleteMutation = useMutation<
    FetchResponse<Reminder> | undefined,
    Error | FetchError,
    DeleteParams
  >(({ id }) => deleteReminder(id), {
    onSuccess: () => {
      router.push("/reminders");
    },
    onError: (error) => {
      setError(`Error when deleting the resource: ${error}`);
      console.error(error);
    },
  });

  const handleDelete = () => {
    if (!reminder || !reminder["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;
    deleteMutation.mutate({ id: reminder["@id"] });
  };

  return (
    <div className="container mx-auto px-4 max-w-2xl mt-4">
      <Link
        href="/reminders"
        className="text-sm text-cyan-500 font-bold hover:text-cyan-700"
      >
        {`< Back to list`}
      </Link>
      <h1 className="text-3xl my-2">
        {reminder ? `Edit Reminder ${reminder["@id"]}` : `Create Reminder`}
      </h1>
      <Formik
        initialValues={
          reminder
            ? {
                ...reminder,
              }
            : new Reminder()
        }
        validate={() => {
          const errors = {};
          // add your validation logic here
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
          const isCreation = !values["@id"];
          saveMutation.mutate(
            { values },
            {
              onSuccess: () => {
                setStatus({
                  isValid: true,
                  msg: `Element ${isCreation ? "created" : "updated"}.`,
                });
                router.push("/reminders");
              },
              onError: (error) => {
                setStatus({
                  isValid: false,
                  msg: `${error.message}`,
                });
                if ("fields" in error) {
                  setErrors(error.fields);
                }
              },
              onSettled: () => {
                setSubmitting(false);
              },
            }
          );
        }}
      >
        {({
          values,
          status,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form className="shadow-md p-4" onSubmit={handleSubmit}>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="reminder_created_at"
              >
                created_at
              </label>
              <input
                name="created_at"
                id="reminder_created_at"
                value={values.created_at?.toLocaleString() ?? ""}
                type="dateTime"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.created_at && touched.created_at
                    ? "border-red-500"
                    : ""
                }`}
                aria-invalid={
                  errors.created_at && touched.created_at ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="created_at"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="reminder_status"
              >
                status
              </label>
              <input
                name="status"
                id="reminder_status"
                value={values.status ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.status && touched.status ? "border-red-500" : ""
                }`}
                aria-invalid={
                  errors.status && touched.status ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="status"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="reminder_note"
              >
                note
              </label>
              <input
                name="note"
                id="reminder_note"
                value={values.note ?? ""}
                type="text"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.note && touched.note ? "border-red-500" : ""
                }`}
                aria-invalid={errors.note && touched.note ? "true" : undefined}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="note"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="reminder_due_date"
              >
                due_date
              </label>
              <input
                name="due_date"
                id="reminder_due_date"
                value={values.due_date?.toLocaleString() ?? ""}
                type="dateTime"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.due_date && touched.due_date ? "border-red-500" : ""
                }`}
                aria-invalid={
                  errors.due_date && touched.due_date ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="due_date"
              />
            </div>
            <div className="mb-2">
              <div className="text-gray-700 block text-sm font-bold">
                organization
              </div>
              <FieldArray
                name="organization"
                render={(arrayHelpers) => (
                  <div className="mb-2" id="reminder_organization">
                    {values.organization && values.organization.length > 0 ? (
                      values.organization.map((item: any, index: number) => (
                        <div key={index}>
                          <Field name={`organization.${index}`} />
                          <button
                            type="button"
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            -
                          </button>
                          <button
                            type="button"
                            onClick={() => arrayHelpers.insert(index, "")}
                          >
                            +
                          </button>
                        </div>
                      ))
                    ) : (
                      <button
                        type="button"
                        onClick={() => arrayHelpers.push("")}
                      >
                        Add
                      </button>
                    )}
                  </div>
                )}
              />
            </div>
            <div className="mb-2">
              <div className="text-gray-700 block text-sm font-bold">user</div>
              <FieldArray
                name="user"
                render={(arrayHelpers) => (
                  <div className="mb-2" id="reminder_user">
                    {values.user && values.user.length > 0 ? (
                      values.user.map((item: any, index: number) => (
                        <div key={index}>
                          <Field name={`user.${index}`} />
                          <button
                            type="button"
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            -
                          </button>
                          <button
                            type="button"
                            onClick={() => arrayHelpers.insert(index, "")}
                          >
                            +
                          </button>
                        </div>
                      ))
                    ) : (
                      <button
                        type="button"
                        onClick={() => arrayHelpers.push("")}
                      >
                        Add
                      </button>
                    )}
                  </div>
                )}
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="reminder_createdAt"
              >
                createdAt
              </label>
              <input
                name="createdAt"
                id="reminder_createdAt"
                value={values.createdAt?.toLocaleString() ?? ""}
                type="dateTime"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.createdAt && touched.createdAt ? "border-red-500" : ""
                }`}
                aria-invalid={
                  errors.createdAt && touched.createdAt ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="createdAt"
              />
            </div>
            <div className="mb-2">
              <label
                className="text-gray-700 block text-sm font-bold"
                htmlFor="reminder_dueDate"
              >
                dueDate
              </label>
              <input
                name="dueDate"
                id="reminder_dueDate"
                value={values.dueDate?.toLocaleString() ?? ""}
                type="dateTime"
                placeholder=""
                className={`mt-1 block w-full ${
                  errors.dueDate && touched.dueDate ? "border-red-500" : ""
                }`}
                aria-invalid={
                  errors.dueDate && touched.dueDate ? "true" : undefined
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <ErrorMessage
                className="text-xs text-red-500 pt-1"
                component="div"
                name="dueDate"
              />
            </div>
            {status && status.msg && (
              <div
                className={`border px-4 py-3 my-4 rounded ${
                  status.isValid
                    ? "text-cyan-700 border-cyan-500 bg-cyan-200/50"
                    : "text-red-700 border-red-400 bg-red-100"
                }`}
                role="alert"
              >
                {status.msg}
              </div>
            )}
            <button
              type="submit"
              className="inline-block mt-2 bg-cyan-500 hover:bg-cyan-700 text-sm text-white font-bold py-2 px-4 rounded"
              disabled={isSubmitting}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
      <div className="flex space-x-2 mt-4 justify-end">
        {reminder && (
          <button
            className="inline-block mt-2 border-2 border-red-400 hover:border-red-700 hover:text-red-700 text-sm text-red-400 font-bold py-2 px-4 rounded"
            onClick={handleDelete}
          >
            Delete
          </button>
        )}
      </div>
    </div>
  );
};
