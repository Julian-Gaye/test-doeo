import { FunctionComponent, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import Head from "next/head";

import ReferenceLinks from "../common/ReferenceLinks";
import { fetch, getItemPath } from "../../utils/dataAccess";
import { Reminder } from "../../types/Reminder";

interface Props {
  reminder: Reminder;
  text: string;
}

export const Show: FunctionComponent<Props> = ({ reminder, text }) => {
  const [error, setError] = useState<string | null>(null);
  const router = useRouter();

  const handleDelete = async () => {
    if (!reminder["@id"]) return;
    if (!window.confirm("Are you sure you want to delete this item?")) return;

    try {
      await fetch(reminder["@id"], { method: "DELETE" });
      router.push("/reminders");
    } catch (error) {
      setError("Error when deleting the resource.");
      console.error(error);
    }
  };

  return (
    <div className="p-4">
      <Head>
        <title>{`Show Reminder ${reminder["@id"]}`}</title>
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{ __html: text }}
        />
      </Head>
      <Link
        href="/reminders"
        className="text-sm text-cyan-500 font-bold hover:text-cyan-700"
      >
        {"< Back to list"}
      </Link>
      <h1 className="text-3xl mb-2">{`Show Reminder ${reminder["@id"]}`}</h1>
      <table
        cellPadding={10}
        className="shadow-md table border-collapse min-w-full leading-normal table-auto text-left my-3"
      >
        <thead className="w-full text-xs uppercase font-light text-gray-700 bg-gray-200 py-2 px-4">
          <tr>
            <th>Field</th>
            <th>Value</th>
          </tr>
        </thead>
        <tbody className="text-sm divide-y divide-gray-200">
          <tr>
            <th scope="row">created_at</th>
            <td>{reminder["created_at"]?.toLocaleString()}</td>
          </tr>
          <tr>
            <th scope="row">status</th>
            <td>{reminder["status"]}</td>
          </tr>
          <tr>
            <th scope="row">note</th>
            <td>{reminder["note"]}</td>
          </tr>
          <tr>
            <th scope="row">due_date</th>
            <td>{reminder["due_date"]?.toLocaleString()}</td>
          </tr>
          <tr>
            <th scope="row">organization</th>
            <td>
              <ReferenceLinks
                items={reminder["organization"].map((ref: any) => ({
                  href: getItemPath(ref, "/organizations/[id]"),
                  name: ref,
                }))}
              />
            </td>
          </tr>
          <tr>
            <th scope="row">user</th>
            <td>
              <ReferenceLinks
                items={reminder["user"].map((ref: any) => ({
                  href: getItemPath(ref, "/userdoeos/[id]"),
                  name: ref,
                }))}
              />
            </td>
          </tr>
          <tr>
            <th scope="row">createdAt</th>
            <td>{reminder["createdAt"]?.toLocaleString()}</td>
          </tr>
          <tr>
            <th scope="row">dueDate</th>
            <td>{reminder["dueDate"]?.toLocaleString()}</td>
          </tr>
        </tbody>
      </table>
      {error && (
        <div
          className="border px-4 py-3 my-4 rounded text-red-700 border-red-400 bg-red-100"
          role="alert"
        >
          {error}
        </div>
      )}
      <div className="flex space-x-2 mt-4 items-center justify-end">
        <Link
          href={getItemPath(reminder["@id"], "/reminders/[id]/edit")}
          className="inline-block mt-2 border-2 border-cyan-500 bg-cyan-500 hover:border-cyan-700 hover:bg-cyan-700 text-xs text-white font-bold py-2 px-4 rounded"
        >
          Edit
        </Link>
        <button
          className="inline-block mt-2 border-2 border-red-400 hover:border-red-700 hover:text-red-700 text-xs text-red-400 font-bold py-2 px-4 rounded"
          onClick={handleDelete}
        >
          Delete
        </button>
      </div>
    </div>
  );
};
