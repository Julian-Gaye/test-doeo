import { Item } from "./item";

export class AppUser implements Item {
  public "@id"?: string;

  constructor(
    _id?: string,
    public last_name?: string,
    public first_name?: string,
    public status?: string,
    public last_login?: Date,
    public created_at?: Date,
    public deleted_at?: Date,
    public organization?: string[],
    public lastName?: string,
    public firstName?: string,
    public lastLogin?: Date,
    public createdAt?: Date,
    public deletedAt?: Date
  ) {
    this["@id"] = _id;
  }
}
