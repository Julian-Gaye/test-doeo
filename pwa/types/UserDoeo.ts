import { Item } from "./item";

export class UserDoeo implements Item {
  public "@id"?: string;

  constructor(
    _id?: string,
    public username?: string,
    public fullname?: string,
    public email?: string,
    public talks?: string[],
    public reminders?: string[]
  ) {
    this["@id"] = _id;
  }
}
