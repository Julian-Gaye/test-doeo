import { Item } from "./item";

export class Organization implements Item {
  public "@id"?: string;

  constructor(
    _id?: string,
    public name?: string,
    public status?: string,
    public referent_name?: string,
    public referent_function?: string,
    public referent_mail?: string,
    public referent_phone?: string,
    public talks?: string[],
    public reminders?: string[],
    public appUsers?: string[],
    public referentName?: string,
    public referentFunction?: string,
    public referentMail?: string,
    public referentPhone?: string
  ) {
    this["@id"] = _id;
  }
}
