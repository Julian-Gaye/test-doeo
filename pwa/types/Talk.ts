import { Item } from "./item";

export class Talk implements Item {
  public "@id"?: string;

  constructor(
    _id?: string,
    public user?: string[],
    public organization?: string[],
    public datetime?: Date,
    public note?: string
  ) {
    this["@id"] = _id;
  }
}
