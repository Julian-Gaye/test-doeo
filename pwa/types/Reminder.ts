import { Item } from "./item";

export class Reminder implements Item {
  public "@id"?: string;

  constructor(
    _id?: string,
    public created_at?: Date,
    public status?: string,
    public note?: string,
    public due_date?: Date,
    public organization?: string[],
    public user?: string[],
    public createdAt?: Date,
    public dueDate?: Date
  ) {
    this["@id"] = _id;
  }
}
